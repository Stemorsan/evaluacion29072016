﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CDatos;
using System.Windows.Forms;

namespace CLogica
{
    class Diagnostico
    {
        private long idPaciente;
        private string nombres;
        private string apellidos;
        private long idDoctor;
        private string nombresDoc;
        private string apellidosDoc;
        private string diagnostico;
        private string medicamentos;

        public SqlDataAdapter Clt;
        public DataTable TDiagn;

        string sentencia;
        Conexion oConex = new Conexion();
        #region Atributos
        public long IdPaciente
        {
            get
            {
                return idPaciente;
            }

            set
            {
                idPaciente = value;
            }
        }

        public string Nombres
        {
            get
            {
                return nombres;
            }

            set
            {
                nombres = value;
            }
        }

        public string Apellidos
        {
            get
            {
                return apellidos;
            }

            set
            {
                apellidos = value;
            }
        }

        public long IdDoctor
        {
            get
            {
                return idDoctor;
            }

            set
            {
                idDoctor = value;
            }
        }

        public string NombresDoc
        {
            get
            {
                return nombresDoc;
            }

            set
            {
                nombresDoc = value;
            }
        }

        public string ApellidosDoc
        {
            get
            {
                return apellidosDoc;
            }

            set
            {
                apellidosDoc = value;
            }
        }

        public string Diagnostico
        {
            get
            {
                return diagnostico;
            }

            set
            {
                diagnostico = value;
            }
        }

        public string Medicamentos
        {
            get
            {
                return medicamentos;
            }

            set
            {
                medicamentos = value;
            }
        }
        #endregion 
        public void RealizarDiagn(long IdPaciente, string Nombres,string Apellidos, long IdDoctor,string NombresDoc, string ApellidosDoc,string Diagnostico,string Medicamentos)
        {
            sentencia = "exec RegisDiagnost" + IdPaciente + ",'" + Nombres + ",'" + Apellidos + ",'" + IdDoctor + ",'" + NombresDoc + ",'" + ApellidosDoc + ",'" + Diagnostico + ",'" + Medicamentos + "";
            oConex.ejecutar_sentencias_sql(sentencia);
        }
        public void ModifDiagn(long IdPaciente, string Nombres, string Apellidos, long IdDoctor, string NombresDoc, string ApellidosDoc, string Diagnostico, string Medicamentos)
        {
            sentencia = "exec ModifDiagnost" + IdPaciente + ",'" + Nombres + ",'" + Apellidos + ",'" + IdDoctor + ",'" + NombresDoc + ",'" + ApellidosDoc + ",'" + Diagnostico + ",'" + Medicamentos + "";
            oConex.ejecutar_sentencias_sql(sentencia);
        }
        public void EliminDiagn(long IdPaciente)
        {
            sentencia = "exec ElimDiagnost" + IdPaciente + "";
            oConex.ejecutar_sentencias_sql(sentencia);
        }
        public DataTable Consultar_Todos_Pacientes()
        {
            try
            {
                Clt = new SqlDataAdapter("select * from diagnostico", oConex.conecte());
                TDiagn = new DataTable();
                Clt.Fill(TDiagn);
                return TDiagn;

            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error al cargar datos" + ex);
                return TDiagn;
            }
        }
    }
}
