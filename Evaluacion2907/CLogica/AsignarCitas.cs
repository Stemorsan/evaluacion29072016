﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using CDatos;
using System.Windows.Forms;

namespace CLogica
{
    class AsignarCitas
    {
        private long idPaciente;
        private string nombres;
        private string apellidos;
        private string nombreEmpl;
        private string nickNameEmp;
        private string claveEmp;
        private DateTime fechaCita;

        public SqlDataAdapter Clt;
        public DataTable TPaciente;

        string sentencia;
        Conexion oConex = new Conexion();

        #region Atributos
        public long IdPaciente
        {
            get
            {
                return idPaciente;
            }

            set
            {
                idPaciente = value;
            }
        }

        public string Nombres
        {
            get
            {
                return nombres;
            }

            set
            {
                nombres = value;
            }
        }

        public string Apellidos
        {
            get
            {
                return apellidos;
            }

            set
            {
                apellidos = value;
            }
        }

        public string NombreEmpl
        {
            get
            {
                return nombreEmpl;
            }

            set
            {
                nombreEmpl = value;
            }
        }

        public string NickNameEmp
        {
            get
            {
                return nickNameEmp;
            }

            set
            {
                nickNameEmp = value;
            }
        }

        public string ClaveEmp
        {
            get
            {
                return claveEmp;
            }

            set
            {
                claveEmp = value;
            }
        }

        public DateTime FechaCita
        {
            get
            {
                return fechaCita;
            }

            set
            {
                fechaCita = value;
            }
        }
        #endregion

        public void AsignarCita(long IdPaciente, string Nombres, string Apellidos, string NombreEmpl, string nickNameEmp, string claveEmp, DateTime FechaCita)
        {
            sentencia = "exec AsigCitas" + IdPaciente + ",'" + Nombres + ",'" + Apellidos + ",'" + NombreEmpl + ",'" + nickNameEmp + ",'" + claveEmp + ",'" + FechaCita + "";
            oConex.ejecutar_sentencias_sql(sentencia);
        }
        public void ModificarCita(long IdPaciente, string Nombres, string Apellidos, string NombreEmpl, string nickNameEmp, string claveEmp, DateTime FechaCita)
        {
            sentencia = "exec ModifCitas"+ IdPaciente + ",'" + Nombres + ",'" + Apellidos + ",'" + NombreEmpl + ",'" + nickNameEmp + ",'" + claveEmp + ",'" + FechaCita + "";
            oConex.ejecutar_sentencias_sql(sentencia);
        }
        public void CancelarCitas( long IdPaciente)
        {
            sentencia = "exec ElimDatos" + IdPaciente + "";
            oConex.ejecutar_sentencias_sql(sentencia);
        }
        public DataTable Consultar_Todos_Pacientes()
        {
            try
            {
                Clt = new SqlDataAdapter("select * from citas", oConex.conecte());
                TPaciente = new DataTable();
                Clt.Fill(TPaciente);
                return TPaciente;

            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error al cargar datos" + ex);
                return TPaciente;
            }
        }
    }
}
