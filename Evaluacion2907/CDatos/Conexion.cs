﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;



namespace CDatos
{
    public class Conexion
    {
        SqlConnection con = new SqlConnection("Data Source=DESKTOP-Q3AODOA;Initial Catalog=Evaluacion2907;Integrated Security=True");

        public SqlDataReader lector;
        public DataTable tabla = new DataTable();
        public SqlCommand consulta = new SqlCommand();
        protected string sql;
        public void conectar()
        {

            try
            {

                consulta.Connection = con;
                con.Open();
            }

            catch
            {

                MessageBox.Show("No se puede conectar con la base de datos comniquese con su administrador", "Conexion Fallida", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);

                desconectar();
                throw;
            }
        }
        private void desconectar()
        {
            con.Close();
        }

        public SqlConnection conec;

        public SqlConnection conecte()
        {
            try
            {
                conec = new SqlConnection();
                conec.Open();
                return conec;
            }catch(SqlException ex)
            {
                MessageBox.Show("Error al conectar" + ex);
                conec.Close();
                return conec;
            }

        }
     

        public void ejecutar_sentencias_sql(string sentencia)
        {
            try
            {
                consulta.CommandText = sentencia;
                conecte();
                consulta.ExecuteNonQuery();
                desconectar();
            }
            catch(Exception ex)
            {
                MessageBox.Show("No se puede ejecutar la sentencia de SQL" + ex + "", "Error Sentencia SQL", MessageBoxButtons.OK, MessageBoxIcon.Error);
                desconectar();
            }
        }


    }
}
