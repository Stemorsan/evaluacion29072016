﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Evaluacion2907
{
    public partial class RegistroEmp : Form
    {
        public RegistroEmp()
        {
            InitializeComponent();
        }

        private void btnRegistrarse_Click(object sender, EventArgs e)
        {
            LoginEmpleado oLogEmp = new LoginEmpleado();
            oLogEmp.Show();
            this.Hide();
        }
    }
}
