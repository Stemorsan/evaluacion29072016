﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Evaluacion2907
{
    public partial class LoginEmpleado : Form
    {
        public LoginEmpleado()
        {
            InitializeComponent();
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            CLogica.LoginEmp oLoginemp = new CLogica.LoginEmp();
            string select = oLoginemp.Muestra_Usuario(txtNickNameEmp.Text, txtClaveEmp.Text);

            if (select != "")
            {
                AsignarCitas formasg = new AsignarCitas();
                formasg.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Usuario o clave no validos");
                return;
            }
        }

        private void btnRegistrarse_Click(object sender, EventArgs e)
        {
            RegistroEmp oreg = new RegistroEmp();
            oreg.Show();
            this.Hide();
        }
    }
}
