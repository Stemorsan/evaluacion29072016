﻿namespace Evaluacion2907
{
    partial class RegistroEmp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIdEmpl = new System.Windows.Forms.Label();
            this.lblNombreEmp = new System.Windows.Forms.Label();
            this.lblApellidoEmp = new System.Windows.Forms.Label();
            this.lblUsuarioEmp = new System.Windows.Forms.Label();
            this.lblClaveEmp = new System.Windows.Forms.Label();
            this.btnRegistrarse = new System.Windows.Forms.Button();
            this.txtIdEmp = new System.Windows.Forms.TextBox();
            this.txtNombreEmp = new System.Windows.Forms.TextBox();
            this.txtApellidoEmp = new System.Windows.Forms.TextBox();
            this.txtUsuarioEmp = new System.Windows.Forms.TextBox();
            this.txtClaveEmp = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblIdEmpl
            // 
            this.lblIdEmpl.AutoSize = true;
            this.lblIdEmpl.Location = new System.Drawing.Point(37, 31);
            this.lblIdEmpl.Name = "lblIdEmpl";
            this.lblIdEmpl.Size = new System.Drawing.Size(19, 13);
            this.lblIdEmpl.TabIndex = 0;
            this.lblIdEmpl.Text = "Id ";
            // 
            // lblNombreEmp
            // 
            this.lblNombreEmp.AutoSize = true;
            this.lblNombreEmp.Location = new System.Drawing.Point(37, 74);
            this.lblNombreEmp.Name = "lblNombreEmp";
            this.lblNombreEmp.Size = new System.Drawing.Size(44, 13);
            this.lblNombreEmp.TabIndex = 1;
            this.lblNombreEmp.Text = "Nombre";
            // 
            // lblApellidoEmp
            // 
            this.lblApellidoEmp.AutoSize = true;
            this.lblApellidoEmp.Location = new System.Drawing.Point(37, 121);
            this.lblApellidoEmp.Name = "lblApellidoEmp";
            this.lblApellidoEmp.Size = new System.Drawing.Size(44, 13);
            this.lblApellidoEmp.TabIndex = 2;
            this.lblApellidoEmp.Text = "Apellido";
            // 
            // lblUsuarioEmp
            // 
            this.lblUsuarioEmp.AutoSize = true;
            this.lblUsuarioEmp.Location = new System.Drawing.Point(37, 165);
            this.lblUsuarioEmp.Name = "lblUsuarioEmp";
            this.lblUsuarioEmp.Size = new System.Drawing.Size(43, 13);
            this.lblUsuarioEmp.TabIndex = 3;
            this.lblUsuarioEmp.Text = "Usuario";
            // 
            // lblClaveEmp
            // 
            this.lblClaveEmp.AutoSize = true;
            this.lblClaveEmp.Location = new System.Drawing.Point(37, 202);
            this.lblClaveEmp.Name = "lblClaveEmp";
            this.lblClaveEmp.Size = new System.Drawing.Size(34, 13);
            this.lblClaveEmp.TabIndex = 4;
            this.lblClaveEmp.Text = "Clave";
            // 
            // btnRegistrarse
            // 
            this.btnRegistrarse.Location = new System.Drawing.Point(227, 251);
            this.btnRegistrarse.Name = "btnRegistrarse";
            this.btnRegistrarse.Size = new System.Drawing.Size(71, 27);
            this.btnRegistrarse.TabIndex = 5;
            this.btnRegistrarse.Text = "Registrarse";
            this.btnRegistrarse.UseVisualStyleBackColor = true;
            this.btnRegistrarse.Click += new System.EventHandler(this.btnRegistrarse_Click);
            // 
            // txtIdEmp
            // 
            this.txtIdEmp.Location = new System.Drawing.Point(105, 28);
            this.txtIdEmp.Name = "txtIdEmp";
            this.txtIdEmp.Size = new System.Drawing.Size(100, 20);
            this.txtIdEmp.TabIndex = 6;
            // 
            // txtNombreEmp
            // 
            this.txtNombreEmp.Location = new System.Drawing.Point(105, 71);
            this.txtNombreEmp.Name = "txtNombreEmp";
            this.txtNombreEmp.Size = new System.Drawing.Size(100, 20);
            this.txtNombreEmp.TabIndex = 7;
            // 
            // txtApellidoEmp
            // 
            this.txtApellidoEmp.Location = new System.Drawing.Point(105, 118);
            this.txtApellidoEmp.Name = "txtApellidoEmp";
            this.txtApellidoEmp.Size = new System.Drawing.Size(100, 20);
            this.txtApellidoEmp.TabIndex = 8;
            // 
            // txtUsuarioEmp
            // 
            this.txtUsuarioEmp.Location = new System.Drawing.Point(105, 162);
            this.txtUsuarioEmp.Name = "txtUsuarioEmp";
            this.txtUsuarioEmp.Size = new System.Drawing.Size(100, 20);
            this.txtUsuarioEmp.TabIndex = 9;
            // 
            // txtClaveEmp
            // 
            this.txtClaveEmp.Location = new System.Drawing.Point(105, 199);
            this.txtClaveEmp.Name = "txtClaveEmp";
            this.txtClaveEmp.Size = new System.Drawing.Size(100, 20);
            this.txtClaveEmp.TabIndex = 10;
            // 
            // RegistroEmp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(322, 310);
            this.Controls.Add(this.txtClaveEmp);
            this.Controls.Add(this.txtUsuarioEmp);
            this.Controls.Add(this.txtApellidoEmp);
            this.Controls.Add(this.txtNombreEmp);
            this.Controls.Add(this.txtIdEmp);
            this.Controls.Add(this.btnRegistrarse);
            this.Controls.Add(this.lblClaveEmp);
            this.Controls.Add(this.lblUsuarioEmp);
            this.Controls.Add(this.lblApellidoEmp);
            this.Controls.Add(this.lblNombreEmp);
            this.Controls.Add(this.lblIdEmpl);
            this.Name = "RegistroEmp";
            this.Text = "Registro Empleado";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblIdEmpl;
        private System.Windows.Forms.Label lblNombreEmp;
        private System.Windows.Forms.Label lblApellidoEmp;
        private System.Windows.Forms.Label lblUsuarioEmp;
        private System.Windows.Forms.Label lblClaveEmp;
        private System.Windows.Forms.Button btnRegistrarse;
        private System.Windows.Forms.TextBox txtIdEmp;
        private System.Windows.Forms.TextBox txtNombreEmp;
        private System.Windows.Forms.TextBox txtApellidoEmp;
        private System.Windows.Forms.TextBox txtUsuarioEmp;
        private System.Windows.Forms.TextBox txtClaveEmp;
    }
}