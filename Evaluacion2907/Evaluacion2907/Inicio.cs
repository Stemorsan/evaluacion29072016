﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Evaluacion2907
{
    public partial class Inicio : Form
    {
        public Inicio()
        {
            InitializeComponent();
        }

        private void btnDoc_Click(object sender, EventArgs e)
        {
            LoginDoctor oformd = new LoginDoctor();
            oformd.Show();
            this.Hide();
        }

        private void btnEmp_Click(object sender, EventArgs e)
        {
            LoginEmpleado oforme = new LoginEmpleado();
            oforme.Show();
            this.Hide();
        }
    }
}
