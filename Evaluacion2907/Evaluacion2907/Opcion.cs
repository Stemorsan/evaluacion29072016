﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Evaluacion2907
{
    public partial class Opcion : Form
    {
        public Opcion()
        {
            InitializeComponent();
        }

        private void btnRelizarDiag_Click(object sender, EventArgs e)
        {
            Diagnostico oDiag = new Diagnostico();
            oDiag.Show();
            this.Hide();
        }

        private void btnVerHist_Click(object sender, EventArgs e)
        {
            Historial oHis = new Historial();
            oHis.Show();
            this.Hide();
        }
    }
}
