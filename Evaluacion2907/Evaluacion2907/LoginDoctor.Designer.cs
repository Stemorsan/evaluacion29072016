﻿namespace Evaluacion2907
{
    partial class LoginDoctor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnIngresar = new System.Windows.Forms.Button();
            this.btnRegistrarse = new System.Windows.Forms.Button();
            this.lblNickNameDoc = new System.Windows.Forms.Label();
            this.lblClaveDoc = new System.Windows.Forms.Label();
            this.txtNickNameDoc = new System.Windows.Forms.TextBox();
            this.txtClaveDoc = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnIngresar
            // 
            this.btnIngresar.Location = new System.Drawing.Point(43, 134);
            this.btnIngresar.Name = "btnIngresar";
            this.btnIngresar.Size = new System.Drawing.Size(75, 23);
            this.btnIngresar.TabIndex = 0;
            this.btnIngresar.Text = "&Ingresar";
            this.btnIngresar.UseVisualStyleBackColor = true;
            this.btnIngresar.Click += new System.EventHandler(this.btnIngresar_Click);
            // 
            // btnRegistrarse
            // 
            this.btnRegistrarse.Location = new System.Drawing.Point(157, 134);
            this.btnRegistrarse.Name = "btnRegistrarse";
            this.btnRegistrarse.Size = new System.Drawing.Size(75, 23);
            this.btnRegistrarse.TabIndex = 1;
            this.btnRegistrarse.Text = "Registrarse";
            this.btnRegistrarse.UseVisualStyleBackColor = true;
            this.btnRegistrarse.Click += new System.EventHandler(this.btnRegistrarse_Click);
            // 
            // lblNickNameDoc
            // 
            this.lblNickNameDoc.AutoSize = true;
            this.lblNickNameDoc.Location = new System.Drawing.Point(40, 34);
            this.lblNickNameDoc.Name = "lblNickNameDoc";
            this.lblNickNameDoc.Size = new System.Drawing.Size(43, 13);
            this.lblNickNameDoc.TabIndex = 2;
            this.lblNickNameDoc.Text = "Usuario";
            // 
            // lblClaveDoc
            // 
            this.lblClaveDoc.AutoSize = true;
            this.lblClaveDoc.Location = new System.Drawing.Point(40, 87);
            this.lblClaveDoc.Name = "lblClaveDoc";
            this.lblClaveDoc.Size = new System.Drawing.Size(34, 13);
            this.lblClaveDoc.TabIndex = 3;
            this.lblClaveDoc.Text = "Clave";
            // 
            // txtNickNameDoc
            // 
            this.txtNickNameDoc.Location = new System.Drawing.Point(132, 31);
            this.txtNickNameDoc.Name = "txtNickNameDoc";
            this.txtNickNameDoc.Size = new System.Drawing.Size(100, 20);
            this.txtNickNameDoc.TabIndex = 4;
            // 
            // txtClaveDoc
            // 
            this.txtClaveDoc.Location = new System.Drawing.Point(132, 84);
            this.txtClaveDoc.Name = "txtClaveDoc";
            this.txtClaveDoc.Size = new System.Drawing.Size(100, 20);
            this.txtClaveDoc.TabIndex = 5;
            // 
            // LoginDoctor
            // 
            this.AcceptButton = this.btnIngresar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(274, 203);
            this.Controls.Add(this.txtClaveDoc);
            this.Controls.Add(this.txtNickNameDoc);
            this.Controls.Add(this.lblClaveDoc);
            this.Controls.Add(this.lblNickNameDoc);
            this.Controls.Add(this.btnRegistrarse);
            this.Controls.Add(this.btnIngresar);
            this.Name = "LoginDoctor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LoginDoctor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnIngresar;
        private System.Windows.Forms.Button btnRegistrarse;
        private System.Windows.Forms.Label lblNickNameDoc;
        private System.Windows.Forms.Label lblClaveDoc;
        private System.Windows.Forms.TextBox txtNickNameDoc;
        private System.Windows.Forms.TextBox txtClaveDoc;
    }
}