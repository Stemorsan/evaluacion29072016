﻿namespace Evaluacion2907
{
    partial class Diagnostico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIdPaciente = new System.Windows.Forms.Label();
            this.lblNombresPac = new System.Windows.Forms.Label();
            this.lblApellidosPac = new System.Windows.Forms.Label();
            this.lblIdDoc = new System.Windows.Forms.Label();
            this.lblNombresDoc = new System.Windows.Forms.Label();
            this.lblApellidosDoc = new System.Windows.Forms.Label();
            this.lblDiagnostico = new System.Windows.Forms.Label();
            this.lblMedicamentos = new System.Windows.Forms.Label();
            this.txtIdPaciente = new System.Windows.Forms.TextBox();
            this.txtNombrePac = new System.Windows.Forms.TextBox();
            this.txtApellidosPac = new System.Windows.Forms.TextBox();
            this.txtIdDoc = new System.Windows.Forms.TextBox();
            this.txtNombresDoc = new System.Windows.Forms.TextBox();
            this.txtApellidosDoc = new System.Windows.Forms.TextBox();
            this.rtbDiagnostico = new System.Windows.Forms.RichTextBox();
            this.rtbMedicamentos = new System.Windows.Forms.RichTextBox();
            this.btnIngresarDiag = new System.Windows.Forms.Button();
            this.btnModificarDiag = new System.Windows.Forms.Button();
            this.btnElimDiag = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblIdPaciente
            // 
            this.lblIdPaciente.AutoSize = true;
            this.lblIdPaciente.Location = new System.Drawing.Point(12, 25);
            this.lblIdPaciente.Name = "lblIdPaciente";
            this.lblIdPaciente.Size = new System.Drawing.Size(61, 13);
            this.lblIdPaciente.TabIndex = 0;
            this.lblIdPaciente.Text = "Id Paciente";
            // 
            // lblNombresPac
            // 
            this.lblNombresPac.AutoSize = true;
            this.lblNombresPac.Location = new System.Drawing.Point(12, 61);
            this.lblNombresPac.Name = "lblNombresPac";
            this.lblNombresPac.Size = new System.Drawing.Size(94, 13);
            this.lblNombresPac.TabIndex = 1;
            this.lblNombresPac.Text = "Nombres Paciente";
            // 
            // lblApellidosPac
            // 
            this.lblApellidosPac.AutoSize = true;
            this.lblApellidosPac.Location = new System.Drawing.Point(12, 95);
            this.lblApellidosPac.Name = "lblApellidosPac";
            this.lblApellidosPac.Size = new System.Drawing.Size(94, 13);
            this.lblApellidosPac.TabIndex = 2;
            this.lblApellidosPac.Text = "Apellidos Paciente";
            // 
            // lblIdDoc
            // 
            this.lblIdDoc.AutoSize = true;
            this.lblIdDoc.Location = new System.Drawing.Point(12, 130);
            this.lblIdDoc.Name = "lblIdDoc";
            this.lblIdDoc.Size = new System.Drawing.Size(51, 13);
            this.lblIdDoc.TabIndex = 3;
            this.lblIdDoc.Text = "Id Doctor";
            // 
            // lblNombresDoc
            // 
            this.lblNombresDoc.AutoSize = true;
            this.lblNombresDoc.Location = new System.Drawing.Point(12, 166);
            this.lblNombresDoc.Name = "lblNombresDoc";
            this.lblNombresDoc.Size = new System.Drawing.Size(84, 13);
            this.lblNombresDoc.TabIndex = 4;
            this.lblNombresDoc.Text = "Nombres Doctor";
            // 
            // lblApellidosDoc
            // 
            this.lblApellidosDoc.AutoSize = true;
            this.lblApellidosDoc.Location = new System.Drawing.Point(12, 200);
            this.lblApellidosDoc.Name = "lblApellidosDoc";
            this.lblApellidosDoc.Size = new System.Drawing.Size(84, 13);
            this.lblApellidosDoc.TabIndex = 5;
            this.lblApellidosDoc.Text = "Apellidos Doctor";
            // 
            // lblDiagnostico
            // 
            this.lblDiagnostico.AutoSize = true;
            this.lblDiagnostico.Location = new System.Drawing.Point(276, 25);
            this.lblDiagnostico.Name = "lblDiagnostico";
            this.lblDiagnostico.Size = new System.Drawing.Size(63, 13);
            this.lblDiagnostico.TabIndex = 6;
            this.lblDiagnostico.Text = "Diagnostico";
            // 
            // lblMedicamentos
            // 
            this.lblMedicamentos.AutoSize = true;
            this.lblMedicamentos.Location = new System.Drawing.Point(276, 134);
            this.lblMedicamentos.Name = "lblMedicamentos";
            this.lblMedicamentos.Size = new System.Drawing.Size(76, 13);
            this.lblMedicamentos.TabIndex = 7;
            this.lblMedicamentos.Text = "Medicamentos";
            // 
            // txtIdPaciente
            // 
            this.txtIdPaciente.Location = new System.Drawing.Point(118, 22);
            this.txtIdPaciente.Name = "txtIdPaciente";
            this.txtIdPaciente.Size = new System.Drawing.Size(100, 20);
            this.txtIdPaciente.TabIndex = 11;
            // 
            // txtNombrePac
            // 
            this.txtNombrePac.Location = new System.Drawing.Point(118, 58);
            this.txtNombrePac.Name = "txtNombrePac";
            this.txtNombrePac.Size = new System.Drawing.Size(100, 20);
            this.txtNombrePac.TabIndex = 12;
            // 
            // txtApellidosPac
            // 
            this.txtApellidosPac.Location = new System.Drawing.Point(118, 92);
            this.txtApellidosPac.Name = "txtApellidosPac";
            this.txtApellidosPac.Size = new System.Drawing.Size(100, 20);
            this.txtApellidosPac.TabIndex = 13;
            // 
            // txtIdDoc
            // 
            this.txtIdDoc.Location = new System.Drawing.Point(118, 127);
            this.txtIdDoc.Name = "txtIdDoc";
            this.txtIdDoc.Size = new System.Drawing.Size(100, 20);
            this.txtIdDoc.TabIndex = 14;
            // 
            // txtNombresDoc
            // 
            this.txtNombresDoc.Location = new System.Drawing.Point(118, 166);
            this.txtNombresDoc.Name = "txtNombresDoc";
            this.txtNombresDoc.Size = new System.Drawing.Size(100, 20);
            this.txtNombresDoc.TabIndex = 15;
            // 
            // txtApellidosDoc
            // 
            this.txtApellidosDoc.Location = new System.Drawing.Point(118, 197);
            this.txtApellidosDoc.Name = "txtApellidosDoc";
            this.txtApellidosDoc.Size = new System.Drawing.Size(100, 20);
            this.txtApellidosDoc.TabIndex = 16;
            // 
            // rtbDiagnostico
            // 
            this.rtbDiagnostico.Location = new System.Drawing.Point(279, 58);
            this.rtbDiagnostico.Name = "rtbDiagnostico";
            this.rtbDiagnostico.Size = new System.Drawing.Size(382, 54);
            this.rtbDiagnostico.TabIndex = 17;
            this.rtbDiagnostico.Text = "";
            // 
            // rtbMedicamentos
            // 
            this.rtbMedicamentos.Location = new System.Drawing.Point(279, 163);
            this.rtbMedicamentos.Name = "rtbMedicamentos";
            this.rtbMedicamentos.Size = new System.Drawing.Size(382, 54);
            this.rtbMedicamentos.TabIndex = 18;
            this.rtbMedicamentos.Text = "";
            // 
            // btnIngresarDiag
            // 
            this.btnIngresarDiag.Location = new System.Drawing.Point(15, 270);
            this.btnIngresarDiag.Name = "btnIngresarDiag";
            this.btnIngresarDiag.Size = new System.Drawing.Size(75, 27);
            this.btnIngresarDiag.TabIndex = 19;
            this.btnIngresarDiag.Text = "&Ingresar";
            this.btnIngresarDiag.UseVisualStyleBackColor = true;
            // 
            // btnModificarDiag
            // 
            this.btnModificarDiag.Location = new System.Drawing.Point(143, 270);
            this.btnModificarDiag.Name = "btnModificarDiag";
            this.btnModificarDiag.Size = new System.Drawing.Size(75, 27);
            this.btnModificarDiag.TabIndex = 20;
            this.btnModificarDiag.Text = "Modificar";
            this.btnModificarDiag.UseVisualStyleBackColor = true;
            // 
            // btnElimDiag
            // 
            this.btnElimDiag.Location = new System.Drawing.Point(279, 270);
            this.btnElimDiag.Name = "btnElimDiag";
            this.btnElimDiag.Size = new System.Drawing.Size(75, 27);
            this.btnElimDiag.TabIndex = 21;
            this.btnElimDiag.Text = "Eliminar ";
            this.btnElimDiag.UseVisualStyleBackColor = true;
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(400, 270);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 27);
            this.btnSalir.TabIndex = 22;
            this.btnSalir.Text = "&Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            // 
            // Diagnostico
            // 
            this.AcceptButton = this.btnIngresarDiag;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnSalir;
            this.ClientSize = new System.Drawing.Size(713, 629);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnElimDiag);
            this.Controls.Add(this.btnModificarDiag);
            this.Controls.Add(this.btnIngresarDiag);
            this.Controls.Add(this.rtbMedicamentos);
            this.Controls.Add(this.rtbDiagnostico);
            this.Controls.Add(this.txtApellidosDoc);
            this.Controls.Add(this.txtNombresDoc);
            this.Controls.Add(this.txtIdDoc);
            this.Controls.Add(this.txtApellidosPac);
            this.Controls.Add(this.txtNombrePac);
            this.Controls.Add(this.txtIdPaciente);
            this.Controls.Add(this.lblMedicamentos);
            this.Controls.Add(this.lblDiagnostico);
            this.Controls.Add(this.lblApellidosDoc);
            this.Controls.Add(this.lblNombresDoc);
            this.Controls.Add(this.lblIdDoc);
            this.Controls.Add(this.lblApellidosPac);
            this.Controls.Add(this.lblNombresPac);
            this.Controls.Add(this.lblIdPaciente);
            this.Name = "Diagnostico";
            this.Text = "Diagnostico";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblIdPaciente;
        private System.Windows.Forms.Label lblNombresPac;
        private System.Windows.Forms.Label lblApellidosPac;
        private System.Windows.Forms.Label lblIdDoc;
        private System.Windows.Forms.Label lblNombresDoc;
        private System.Windows.Forms.Label lblApellidosDoc;
        private System.Windows.Forms.Label lblDiagnostico;
        private System.Windows.Forms.Label lblMedicamentos;
        private System.Windows.Forms.TextBox txtIdPaciente;
        private System.Windows.Forms.TextBox txtNombrePac;
        private System.Windows.Forms.TextBox txtApellidosPac;
        private System.Windows.Forms.TextBox txtIdDoc;
        private System.Windows.Forms.TextBox txtNombresDoc;
        private System.Windows.Forms.TextBox txtApellidosDoc;
        private System.Windows.Forms.RichTextBox rtbDiagnostico;
        private System.Windows.Forms.RichTextBox rtbMedicamentos;
        private System.Windows.Forms.Button btnIngresarDiag;
        private System.Windows.Forms.Button btnModificarDiag;
        private System.Windows.Forms.Button btnElimDiag;
        private System.Windows.Forms.Button btnSalir;
    }
}