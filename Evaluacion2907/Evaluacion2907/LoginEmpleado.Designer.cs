﻿namespace Evaluacion2907
{
    partial class LoginEmpleado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNickNameEmp = new System.Windows.Forms.Label();
            this.lblClaveEmp = new System.Windows.Forms.Label();
            this.btnIngresar = new System.Windows.Forms.Button();
            this.txtNickNameEmp = new System.Windows.Forms.TextBox();
            this.txtClaveEmp = new System.Windows.Forms.TextBox();
            this.btnRegistrarse = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblNickNameEmp
            // 
            this.lblNickNameEmp.AutoSize = true;
            this.lblNickNameEmp.Location = new System.Drawing.Point(37, 44);
            this.lblNickNameEmp.Name = "lblNickNameEmp";
            this.lblNickNameEmp.Size = new System.Drawing.Size(43, 13);
            this.lblNickNameEmp.TabIndex = 0;
            this.lblNickNameEmp.Text = "Usuario";
            // 
            // lblClaveEmp
            // 
            this.lblClaveEmp.AutoSize = true;
            this.lblClaveEmp.Location = new System.Drawing.Point(37, 92);
            this.lblClaveEmp.Name = "lblClaveEmp";
            this.lblClaveEmp.Size = new System.Drawing.Size(34, 13);
            this.lblClaveEmp.TabIndex = 1;
            this.lblClaveEmp.Text = "Clave";
            // 
            // btnIngresar
            // 
            this.btnIngresar.Location = new System.Drawing.Point(40, 153);
            this.btnIngresar.Name = "btnIngresar";
            this.btnIngresar.Size = new System.Drawing.Size(75, 23);
            this.btnIngresar.TabIndex = 2;
            this.btnIngresar.Text = "&Ingresar";
            this.btnIngresar.UseVisualStyleBackColor = true;
            this.btnIngresar.Click += new System.EventHandler(this.btnIngresar_Click);
            // 
            // txtNickNameEmp
            // 
            this.txtNickNameEmp.Location = new System.Drawing.Point(116, 41);
            this.txtNickNameEmp.Name = "txtNickNameEmp";
            this.txtNickNameEmp.Size = new System.Drawing.Size(100, 20);
            this.txtNickNameEmp.TabIndex = 3;
            // 
            // txtClaveEmp
            // 
            this.txtClaveEmp.Location = new System.Drawing.Point(116, 89);
            this.txtClaveEmp.Name = "txtClaveEmp";
            this.txtClaveEmp.Size = new System.Drawing.Size(100, 20);
            this.txtClaveEmp.TabIndex = 4;
            // 
            // btnRegistrarse
            // 
            this.btnRegistrarse.Location = new System.Drawing.Point(174, 153);
            this.btnRegistrarse.Name = "btnRegistrarse";
            this.btnRegistrarse.Size = new System.Drawing.Size(75, 23);
            this.btnRegistrarse.TabIndex = 5;
            this.btnRegistrarse.Text = "Registrarse";
            this.btnRegistrarse.UseVisualStyleBackColor = true;
            this.btnRegistrarse.Click += new System.EventHandler(this.btnRegistrarse_Click);
            // 
            // LoginEmpleado
            // 
            this.AcceptButton = this.btnIngresar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 213);
            this.Controls.Add(this.btnRegistrarse);
            this.Controls.Add(this.txtClaveEmp);
            this.Controls.Add(this.txtNickNameEmp);
            this.Controls.Add(this.btnIngresar);
            this.Controls.Add(this.lblClaveEmp);
            this.Controls.Add(this.lblNickNameEmp);
            this.Name = "LoginEmpleado";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LoginEmpleado";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNickNameEmp;
        private System.Windows.Forms.Label lblClaveEmp;
        private System.Windows.Forms.Button btnIngresar;
        private System.Windows.Forms.TextBox txtNickNameEmp;
        private System.Windows.Forms.TextBox txtClaveEmp;
        private System.Windows.Forms.Button btnRegistrarse;
    }
}