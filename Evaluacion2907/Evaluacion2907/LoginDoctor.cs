﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Evaluacion2907
{
    public partial class LoginDoctor : Form
    {
        public LoginDoctor()
        {
            InitializeComponent();
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            CLogica.LoginDoc ologdoc = new CLogica.LoginDoc();
            string select = ologdoc.Muestra_Usuario(txtNickNameDoc.Text, txtClaveDoc.Text);

            if (select != "")
            {
                Opcion oOpc = new Opcion();
                oOpc.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Usuario o clave no validos");
                return;
            }
       }

        private void btnRegistrarse_Click(object sender, EventArgs e)
        {
            RegistroDoc oreg = new RegistroDoc();
            oreg.Show();
            this.Hide();
        }
    }
}
