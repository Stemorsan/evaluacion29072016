﻿namespace Evaluacion2907
{
    partial class RegistroDoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.lblIdDoc = new System.Windows.Forms.Label();
            this.lblNombresDoc = new System.Windows.Forms.Label();
            this.lblApellidosDoc = new System.Windows.Forms.Label();
            this.lblUsuarioDoc = new System.Windows.Forms.Label();
            this.lblClaveDoc = new System.Windows.Forms.Label();
            this.btnregistrarse = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(104, 26);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 0;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(104, 63);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 1;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(104, 99);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 2;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(104, 135);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 3;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(104, 170);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 4;
            // 
            // lblIdDoc
            // 
            this.lblIdDoc.AutoSize = true;
            this.lblIdDoc.Location = new System.Drawing.Point(25, 29);
            this.lblIdDoc.Name = "lblIdDoc";
            this.lblIdDoc.Size = new System.Drawing.Size(16, 13);
            this.lblIdDoc.TabIndex = 5;
            this.lblIdDoc.Text = "Id";
            // 
            // lblNombresDoc
            // 
            this.lblNombresDoc.AutoSize = true;
            this.lblNombresDoc.Location = new System.Drawing.Point(25, 66);
            this.lblNombresDoc.Name = "lblNombresDoc";
            this.lblNombresDoc.Size = new System.Drawing.Size(49, 13);
            this.lblNombresDoc.TabIndex = 6;
            this.lblNombresDoc.Text = "Nombres";
            // 
            // lblApellidosDoc
            // 
            this.lblApellidosDoc.AutoSize = true;
            this.lblApellidosDoc.Location = new System.Drawing.Point(25, 102);
            this.lblApellidosDoc.Name = "lblApellidosDoc";
            this.lblApellidosDoc.Size = new System.Drawing.Size(49, 13);
            this.lblApellidosDoc.TabIndex = 7;
            this.lblApellidosDoc.Text = "Apellidos";
            // 
            // lblUsuarioDoc
            // 
            this.lblUsuarioDoc.AutoSize = true;
            this.lblUsuarioDoc.Location = new System.Drawing.Point(25, 138);
            this.lblUsuarioDoc.Name = "lblUsuarioDoc";
            this.lblUsuarioDoc.Size = new System.Drawing.Size(43, 13);
            this.lblUsuarioDoc.TabIndex = 8;
            this.lblUsuarioDoc.Text = "Usuario";
            // 
            // lblClaveDoc
            // 
            this.lblClaveDoc.AutoSize = true;
            this.lblClaveDoc.Location = new System.Drawing.Point(25, 173);
            this.lblClaveDoc.Name = "lblClaveDoc";
            this.lblClaveDoc.Size = new System.Drawing.Size(34, 13);
            this.lblClaveDoc.TabIndex = 9;
            this.lblClaveDoc.Text = "Clave";
            // 
            // btnregistrarse
            // 
            this.btnregistrarse.Location = new System.Drawing.Point(221, 234);
            this.btnregistrarse.Name = "btnregistrarse";
            this.btnregistrarse.Size = new System.Drawing.Size(75, 23);
            this.btnregistrarse.TabIndex = 10;
            this.btnregistrarse.Text = "Registrarse";
            this.btnregistrarse.UseVisualStyleBackColor = true;
            this.btnregistrarse.Click += new System.EventHandler(this.btnregistrarse_Click);
            // 
            // RegistroDoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(325, 286);
            this.Controls.Add(this.btnregistrarse);
            this.Controls.Add(this.lblClaveDoc);
            this.Controls.Add(this.lblUsuarioDoc);
            this.Controls.Add(this.lblApellidosDoc);
            this.Controls.Add(this.lblNombresDoc);
            this.Controls.Add(this.lblIdDoc);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Name = "RegistroDoc";
            this.Text = "Registro Doctor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label lblIdDoc;
        private System.Windows.Forms.Label lblNombresDoc;
        private System.Windows.Forms.Label lblApellidosDoc;
        private System.Windows.Forms.Label lblUsuarioDoc;
        private System.Windows.Forms.Label lblClaveDoc;
        private System.Windows.Forms.Button btnregistrarse;
    }
}