﻿namespace Evaluacion2907
{
    partial class Opcion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnVerHist = new System.Windows.Forms.Button();
            this.btnRelizarDiag = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnVerHist
            // 
            this.btnVerHist.Location = new System.Drawing.Point(12, 31);
            this.btnVerHist.Name = "btnVerHist";
            this.btnVerHist.Size = new System.Drawing.Size(121, 23);
            this.btnVerHist.TabIndex = 0;
            this.btnVerHist.Text = "Ver Historial";
            this.btnVerHist.UseVisualStyleBackColor = true;
            this.btnVerHist.Click += new System.EventHandler(this.btnVerHist_Click);
            // 
            // btnRelizarDiag
            // 
            this.btnRelizarDiag.Location = new System.Drawing.Point(159, 31);
            this.btnRelizarDiag.Name = "btnRelizarDiag";
            this.btnRelizarDiag.Size = new System.Drawing.Size(132, 23);
            this.btnRelizarDiag.TabIndex = 1;
            this.btnRelizarDiag.Text = "Realizar Diagnostico";
            this.btnRelizarDiag.UseVisualStyleBackColor = true;
            this.btnRelizarDiag.Click += new System.EventHandler(this.btnRelizarDiag_Click);
            // 
            // Opcion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 82);
            this.Controls.Add(this.btnRelizarDiag);
            this.Controls.Add(this.btnVerHist);
            this.Name = "Opcion";
            this.Text = "Historial";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnVerHist;
        private System.Windows.Forms.Button btnRelizarDiag;
    }
}