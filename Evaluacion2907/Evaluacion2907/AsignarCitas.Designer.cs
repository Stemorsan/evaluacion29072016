﻿namespace Evaluacion2907
{
    partial class AsignarCitas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIdPaciente = new System.Windows.Forms.Label();
            this.lblNombrePac = new System.Windows.Forms.Label();
            this.lblApellidosPac = new System.Windows.Forms.Label();
            this.lblFechaCita = new System.Windows.Forms.Label();
            this.txtIdPaciente = new System.Windows.Forms.TextBox();
            this.txtNombrePac = new System.Windows.Forms.TextBox();
            this.txtApellidoPac = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.btnAsigCita = new System.Windows.Forms.Button();
            this.btnModCita = new System.Windows.Forms.Button();
            this.btnCancelarCita = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblIdPaciente
            // 
            this.lblIdPaciente.AutoSize = true;
            this.lblIdPaciente.Location = new System.Drawing.Point(12, 19);
            this.lblIdPaciente.Name = "lblIdPaciente";
            this.lblIdPaciente.Size = new System.Drawing.Size(61, 13);
            this.lblIdPaciente.TabIndex = 0;
            this.lblIdPaciente.Text = "Id Paciente";
            // 
            // lblNombrePac
            // 
            this.lblNombrePac.AutoSize = true;
            this.lblNombrePac.Location = new System.Drawing.Point(12, 62);
            this.lblNombrePac.Name = "lblNombrePac";
            this.lblNombrePac.Size = new System.Drawing.Size(49, 13);
            this.lblNombrePac.TabIndex = 1;
            this.lblNombrePac.Text = "Nombres";
            // 
            // lblApellidosPac
            // 
            this.lblApellidosPac.AutoSize = true;
            this.lblApellidosPac.Location = new System.Drawing.Point(12, 111);
            this.lblApellidosPac.Name = "lblApellidosPac";
            this.lblApellidosPac.Size = new System.Drawing.Size(49, 13);
            this.lblApellidosPac.TabIndex = 2;
            this.lblApellidosPac.Text = "Apellidos";
            // 
            // lblFechaCita
            // 
            this.lblFechaCita.AutoSize = true;
            this.lblFechaCita.Location = new System.Drawing.Point(12, 165);
            this.lblFechaCita.Name = "lblFechaCita";
            this.lblFechaCita.Size = new System.Drawing.Size(58, 13);
            this.lblFechaCita.TabIndex = 3;
            this.lblFechaCita.Text = "Fecha Cita";
            // 
            // txtIdPaciente
            // 
            this.txtIdPaciente.Location = new System.Drawing.Point(94, 12);
            this.txtIdPaciente.Name = "txtIdPaciente";
            this.txtIdPaciente.Size = new System.Drawing.Size(100, 20);
            this.txtIdPaciente.TabIndex = 4;
            // 
            // txtNombrePac
            // 
            this.txtNombrePac.Location = new System.Drawing.Point(94, 55);
            this.txtNombrePac.Name = "txtNombrePac";
            this.txtNombrePac.Size = new System.Drawing.Size(100, 20);
            this.txtNombrePac.TabIndex = 5;
            // 
            // txtApellidoPac
            // 
            this.txtApellidoPac.Location = new System.Drawing.Point(94, 104);
            this.txtApellidoPac.Name = "txtApellidoPac";
            this.txtApellidoPac.Size = new System.Drawing.Size(100, 20);
            this.txtApellidoPac.TabIndex = 6;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(94, 158);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 7;
            // 
            // btnAsigCita
            // 
            this.btnAsigCita.Location = new System.Drawing.Point(15, 234);
            this.btnAsigCita.Name = "btnAsigCita";
            this.btnAsigCita.Size = new System.Drawing.Size(89, 23);
            this.btnAsigCita.TabIndex = 8;
            this.btnAsigCita.Text = "&Asignar Cita";
            this.btnAsigCita.UseVisualStyleBackColor = true;
            this.btnAsigCita.Click += new System.EventHandler(this.btnAsigCita_Click);
            // 
            // btnModCita
            // 
            this.btnModCita.Location = new System.Drawing.Point(157, 234);
            this.btnModCita.Name = "btnModCita";
            this.btnModCita.Size = new System.Drawing.Size(107, 23);
            this.btnModCita.TabIndex = 9;
            this.btnModCita.Text = "Modificar Cita";
            this.btnModCita.UseVisualStyleBackColor = true;
            // 
            // btnCancelarCita
            // 
            this.btnCancelarCita.Location = new System.Drawing.Point(310, 234);
            this.btnCancelarCita.Name = "btnCancelarCita";
            this.btnCancelarCita.Size = new System.Drawing.Size(96, 23);
            this.btnCancelarCita.TabIndex = 10;
            this.btnCancelarCita.Text = "Cancelar Cita";
            this.btnCancelarCita.UseVisualStyleBackColor = true;
            // 
            // btnSalir
            // 
            this.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnSalir.Location = new System.Drawing.Point(448, 234);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 11;
            this.btnSalir.Text = "&Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            // 
            // AsignarCitas
            // 
            this.AcceptButton = this.btnAsigCita;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnSalir;
            this.ClientSize = new System.Drawing.Size(559, 579);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnCancelarCita);
            this.Controls.Add(this.btnModCita);
            this.Controls.Add(this.btnAsigCita);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.txtApellidoPac);
            this.Controls.Add(this.txtNombrePac);
            this.Controls.Add(this.txtIdPaciente);
            this.Controls.Add(this.lblFechaCita);
            this.Controls.Add(this.lblApellidosPac);
            this.Controls.Add(this.lblNombrePac);
            this.Controls.Add(this.lblIdPaciente);
            this.Name = "AsignarCitas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AsignarCitas";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblIdPaciente;
        private System.Windows.Forms.Label lblNombrePac;
        private System.Windows.Forms.Label lblApellidosPac;
        private System.Windows.Forms.Label lblFechaCita;
        private System.Windows.Forms.TextBox txtIdPaciente;
        private System.Windows.Forms.TextBox txtNombrePac;
        private System.Windows.Forms.TextBox txtApellidoPac;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button btnAsigCita;
        private System.Windows.Forms.Button btnModCita;
        private System.Windows.Forms.Button btnCancelarCita;
        private System.Windows.Forms.Button btnSalir;
    }
}